import React from 'react';
import { useFormik } from 'formik';
import * as Yup from 'yup';
import cn from 'classnames';


interface IProps {
  onSubmit: (formData: { query: string }) => void;
  resetImage: () => void
  onGroupedHandle: () => void
  listLength: number
  loading: boolean
  grouped: boolean
  queryValue: string
}

const SearchForm: React.FC<IProps> = (
  {
    onSubmit, loading, resetImage, onGroupedHandle,
    grouped, listLength, queryValue = ''
  }) => {

  const loginFormOptions = useFormik({
    initialValues: {
      query: queryValue,
    },
    onSubmit: (values) => {
      onSubmit(values);
      resetForm();
    },
    validationSchema: Yup.object().shape({
      query: Yup.string().required('Обязательное поле'),
    }),
    enableReinitialize: true
  });

  const { handleSubmit, handleChange, values, errors, resetForm } = loginFormOptions;

  const SubmitButton = () => {
    return (
      <button
        className={cn('btn', 'btn-primary', 'mr-2')}
        disabled={loading}
      >
        {loading ? 'Загрузка...' : 'Загрузить'}
      </button>
    );
  };

  const ResetButton = () => {
    return (
      <span
        role="button" className={cn(
        'btn', 'btn-danger', 'mr-2',
        { 'disabled': !listLength })} onClick={() => {
        resetImage();
        resetForm();
      }}
      >
        Очистить
      </span>
    );
  };

  const GroupedButton = () => {
    return (
      <span
        role="button" className={cn(
        'btn', 'btn-outline-primary', 'mr-2',
        { 'disabled': !listLength })} onClick={onGroupedHandle}>
        {grouped ? 'Разгруппировать' : 'Сгруппировать'}
      </span>
    );
  };

  return (
    <form onSubmit={handleSubmit} className="d-flex">
      <div className={cn('form-group', 'mb-0', 'mr-2', { 'has-danger': errors['query'] })}>
        <input
          name="query"
          className={cn('form-control ', { 'is-invalid': errors['query'] })}
          placeholder="Поиск..."
          onChange={handleChange}
          value={values['query']}
        />
        {errors['query'] && <span className="invalid-feedback">{errors['query']}</span>}
      </div>

      <div>

        <SubmitButton/>
        <ResetButton/>
        <GroupedButton/>
      </div>
    </form>
  );
};

export default SearchForm;
