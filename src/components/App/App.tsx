import React, { useState } from 'react';

import './bootstrap.min.css';
import SearchForm from '../SearchForm';
import { AppState } from '../../store';
import { connect } from 'react-redux';
import * as actions from '../../store/actions/imageActions';
import Alert from '../Alert';
import ImageList from '../ImageList';
import { ImageType } from '../../types/ImageType';

interface IProps {
  randomImage: (query: string) => void
  resetImage: () => void
  resetImageError: () => void
  setImageTag: (tag: string) => void
  list: ImageType[]
  tags: string[]
  loading: boolean
  error?: string,
  currentTag: string
}

const App: React.FC<IProps> = (
  {
    randomImage, resetImage, list, tags,
    loading,
    error,
    resetImageError,
    setImageTag,
    currentTag
  }) => {

  const [grouped, setGrouped] = useState(false);

  const onSubmit = (formData: { query: string }) => {
    randomImage(formData.query.toLowerCase());
  };

  const onGroupedHandle = () => {
    setGrouped(!grouped);
  };

  return (
    <div className="app container col-6">
      <header className="app__header bg-light p-4 mb-4 navbar align-items-start justify-content-center">
        <span className="navbar-brand">Giphy images</span>
        <SearchForm
          onSubmit={onSubmit}
          resetImage={resetImage}
          loading={loading}
          listLength={list.length}
          grouped={grouped}
          onGroupedHandle={onGroupedHandle}
          queryValue={currentTag}
        />
      </header>

      <main className="app__main">
        <Alert message={error} resetImageError={resetImageError}/>
        <ImageList list={list} tags={tags} grouped={grouped} setImageTag={setImageTag}/>
      </main>
    </div>
  );
};

const mapStateToProps = (state: AppState) => ({
  list: state.image.list,
  tags: state.image.tags,
  loading: state.image.loading,
  error: state.image.error,
  currentTag: state.image.currentTag
});

export default connect(
  mapStateToProps,
  {
    randomImage: actions.randomImage,
    resetImage: actions.resetImage,
    resetImageError: actions.resetImageError,
    setImageTag: actions.setImageTag
  },
)(App);
