import React from 'react';
import { ImageType } from '../../types/ImageType';

import './ImageList.css';

interface IProps {
  list: ImageType[]
  tags: string[]
  grouped: boolean
  setImageTag: (tag: string) => void
}

const ImageList: React.FC<IProps> = ({ list, tags, grouped, setImageTag }) => {
  if (!list.length) {
    return null;
  }

  const tagList = tags.map(tag => {
    return {
      tag,
      list: list.filter(el => el.tag === tag)
    };
  });

  const renderDefaultItems = (
    <div className="row">
      {list.map(({ id, url, tag }) => {
        return (
          <div className="col-4 mb-4" key={id} onClick={() => setImageTag(tag)}>
            <iframe src={url} frameBorder={0} title={tag} className="image-preview"/>
          </div>
        );
      })}
    </div>
  );

  const renderGroupedList = tagList.map(item => {
    return (
      <div className="border border-primary mb-4 p-4" key={item.tag}>
        <h3>{item.tag}</h3>
        <div className="row">
          {item.list.map(({ id, url, tag }) => {
            return (
              <div className="col-4" key={id} onClick={() => setImageTag(tag)}>
                <iframe src={url} frameBorder={0} title={tag} className="image-preview"/>
              </div>
            );
          })}
        </div>
      </div>
    );
  });

  return (
    <>
      <div className="image-list">
        {grouped ? renderGroupedList : renderDefaultItems}
      </div>
    </>
  );
};

export default ImageList;
