import React, { useState, useEffect } from 'react';
import cn from 'classnames';

interface IProps {
  message?: string
  type?: 'info' | 'light' | 'warning'
  resetImageError: () => void
}

const Alert: React.FC<IProps> = ({ message, type = 'info', resetImageError }) => {
  const [text, setText] = useState(message);

  useEffect((): any => {
    if (message) {
      setText(message);
    }
  }, [message]);

  useEffect((): any => {
    if (text) {
      const timer = setTimeout(_ => {
        setText('');
        resetImageError();
      }, 2500);
      return () => clearTimeout(timer);
    }
  }, [text, resetImageError]);

  if (!text) {
    return null;
  }

  return (
    <div className={cn('alert', `alert-${type}`)}>
      {message}
    </div>
  );
};

export default Alert;
