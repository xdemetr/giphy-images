import { ImageActionsTypes } from './imageActionsTypes';

export type AppActions =
  | ImageActionsTypes
