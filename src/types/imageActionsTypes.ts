import { ImageType } from './ImageType';

interface IImageRequested {
  type: 'FETCH_IMAGE_REQUEST'
}

interface IImageLoaded {
  type: 'FETCH_IMAGE_LOADED'
  image: ImageType
}

interface IImageEmpty {
  type: 'FETCH_IMAGE_EMPTY'
}

interface IImageTagAdded {
  type: 'IMAGE_TAG_ADDED'
  tag: string
}

interface IImageAdded {
  type: 'FETCH_IMAGE_ADDED'
  image: ImageType
}

interface IImageReset {
  type: 'IMAGE_RESET'
}

interface IImageErrorClear {
  type: 'IMAGE_ERROR_CLEAR'
}

interface IImageSetTag {
  type: 'IMAGE_SET_TAG',
  tag: string
}

export type ImageActionsTypes =
  | IImageRequested
  | IImageLoaded
  | IImageAdded
  | IImageEmpty
  | IImageTagAdded
  | IImageReset
  | IImageErrorClear
  | IImageSetTag
