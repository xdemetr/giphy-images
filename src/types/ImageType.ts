export interface ImageType {
  url: string
  id: string
  tag: string
}

