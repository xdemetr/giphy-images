import axios from 'axios';

const KEY = 'opprkPj3fSe71ZLj8HT6MPOCOHNdaJkV';
const URL = 'https://api.giphy.com/v1/gifs/';

axios.defaults.baseURL = URL;

export const imageApi = {
  randomImage(query: string): Promise<any> {
    return axios.get('random', {
      params: {
        api_key: KEY,
        tag: query,
      }
    });
  },
};
