import { ImageActionsTypes } from '../../types/imageActionsTypes';
import { ImageType } from '../../types/ImageType';


const initialState = {
  error: '',
  loading: false,
  list: [] as ImageType[],
  tags: [] as string[],
  currentTag: ''
};

type InitialStateType = typeof initialState;

const updateTags = (state: any, tag: string) => {
  let newSelected = [];
  const { tags } = state;
  const existTag = tags.find((t: string) => t === tag);

  if (existTag) {
    newSelected = state.tags;
  } else {
    const newArr = [...tags, tag];
    newSelected = newArr;
  }

  return {
    ...state,
    tags: newSelected,
  };
};

const imageReducer = (state = initialState, action: ImageActionsTypes): InitialStateType => {
  switch (action.type) {
    case 'FETCH_IMAGE_REQUEST':
      return {
        ...state,
        loading: true
      };

    case 'FETCH_IMAGE_LOADED':
      return {
        ...state,
        loading: false,
        list: [action.image, ...state.list],
        currentTag: ''
      };

    case 'FETCH_IMAGE_ADDED':
      return {
        ...state,
        list: [...state.list, action.image],
      };

    case 'FETCH_IMAGE_EMPTY':
      return {
        ...state,
        loading: false,
        error: 'Ничего не найдено'
      };

    case 'IMAGE_TAG_ADDED':
      return updateTags(state, action.tag);

    case 'IMAGE_RESET':
      return {
        ...state,
        list: [],
        tags: [],
        currentTag: ''
      };

    case 'IMAGE_ERROR_CLEAR':
      return {
        ...state,
        error: ''
      };

    case 'IMAGE_SET_TAG':
      return {
        ...state,
        currentTag: action.tag
      };

    default:
      return state;
  }
};

export default imageReducer;
