import { Dispatch } from 'react';

import { AppActions } from '../../types/AppActionsTypes';
import { imageApi } from '../../api/api';

const imageRequested = (): AppActions => ({
  type: 'FETCH_IMAGE_REQUEST'
});

const imageLoaded = (image: any): AppActions => ({
  type: 'FETCH_IMAGE_LOADED',
  image
});

const imageEmpty = (): AppActions => ({
  type: 'FETCH_IMAGE_EMPTY'
});

const imageTagAdded = (tag: string): AppActions => ({
  type: 'IMAGE_TAG_ADDED',
  tag
});

export const randomImage = (
  query: string,
) => async (dispatch: Dispatch<AppActions>) => {
  dispatch(imageRequested());

  try {
    let res = await imageApi.randomImage(query);

    res = {
      id: res.data.data.id,
      url: res.data.data.embed_url,
      tag: query
    };

    if (res.url) {
      dispatch(imageLoaded(res));
      dispatch(imageTagAdded(query));
    } else {
      dispatch(imageEmpty());
    }

  } catch (error) {
    console.log(error);
  }
};

export const resetImage = (): AppActions => ({
  type: 'IMAGE_RESET'
});

export const resetImageError = (): AppActions => ({
  type: 'IMAGE_ERROR_CLEAR'
});

export const setImageTag = (tag: string): AppActions => ({
  type: 'IMAGE_SET_TAG',
  tag
});
