import { applyMiddleware, combineReducers, compose, createStore } from 'redux';
import thunk, { ThunkMiddleware } from 'redux-thunk';

import imageReducer from './reducers/imageReducer';

import { AppActions } from '../types/AppActionsTypes';

const rootReducer = combineReducers({
  image: imageReducer,
});

const composeEnhancers = ((window as any).__REDUX_DEVTOOLS_EXTENSION__ && (window as any).__REDUX_DEVTOOLS_EXTENSION__()) || compose;

export type AppState = ReturnType<typeof rootReducer>;

const store = createStore(
  rootReducer,
  compose(
    applyMiddleware(thunk as ThunkMiddleware<AppState, AppActions>),
    composeEnhancers,
  ),
);

export default store;
